/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.midterm2;

/**
 *
 * @author acer
 */
public class AlineV1 extends Coil {

    public AlineV1(String Usedbrand, String Type, String Wirsize, double Ohm, double Wraps, double Diameter) {
        super(Usedbrand, Type, Wirsize, Ohm, Wraps, Diameter);
        System.out.println("Create AlineV1 Coil");

    }

    @Override
    public void KKVZ() {
        System.out.print("HandMake By : KKVZ ");
    }

    @Override
    public void KKVZ(String Name) {
        System.out.println(Name);
    }

    public void AllInOne() {
        System.out.println("Make By : " + Usedbrand);
        System.out.println("Type Coil : " + Type);
        System.out.println("Wirsize : " + Wirsize);
        System.out.println("Ohm For Coil : " + Ohm);
        System.out.println("Wraps Coil : " + Wraps);
        System.out.println("Diameter Coil : " + Diameter);
    }

    //public void Type() {
    //System.out.println("Type Coil : " + Type);
    //}
    //public void Wirsize() {
    //System.out.println("Wirsize : " + Wirsize);
    //}
    //public void Ohm() {
    //System.out.println("Ohm For Coil : " + Ohm);
    //}
    //public void Wraps() {
    //System.out.println("Wraps Coil : " + Wraps);
    //}
    //public void Diameter() {
    //System.out.println("Diameter Coil : " + Diameter);
    //}
}
