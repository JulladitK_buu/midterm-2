/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.midterm2;

/**
 *
 * @author acer
 */
public class TestCoil {

    public static void main(String[] args) {
        AlineV1 Aline1 = new AlineV1("Twisted Messes", "AlienV1", "27ga*3+36ga*1", 0.11, 5, 2.5);
        Aline1.KKVZ();
        Aline1.KKVZ("Julladit K.");
        Aline1.AllInOne();
        System.out.println("--------------------------");
        AlienV2 Aline2 = new AlienV2("Twisted Messes", "AlienV2", "226ga*3+36ga*1", 0.13, 5, 2.5);
        Aline2.KKVZ();
        Aline2.KKVZ("Pond ");
        Aline2.AllInOne();
        System.out.println("--------------------------");
        AlienAio Aio = new AlienAio("Twisted Messes", "Alien For Aio", "29ga*3+38ga*1", 0.30, 4, 2.5);
        Aio.KKVZ();
        Aio.KKVZ("Nikey ");
        Aio.AllInOne();
        System.out.println("--------------------------");
    }
}
