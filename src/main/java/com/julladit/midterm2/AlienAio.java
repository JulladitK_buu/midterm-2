/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.julladit.midterm2;

/**
 *
 * @author acer
 */
public class AlienAio extends Coil {

    public AlienAio(String Usedbrand, String Type, String Wirsize, double Ohm, double Wraps, double Diameter) {
        super(Usedbrand, Type, Wirsize, Ohm, Wraps, Diameter);
    }

    @Override
    public void KKVZ() {
        System.out.print("HandMake By : KKVZ ");
    }

    @Override
    public void KKVZ(String Name) {
        System.out.println(Name);
    }

    public void AllInOne() {
        System.out.println("Make By : " + Usedbrand);
        System.out.println("Type Coil : " + Type);
        System.out.println("Wirsize : " + Wirsize);
        System.out.println("Ohm For Coil : " + Ohm);
        System.out.println("Wraps Coil : " + Wraps);
        System.out.println("Diameter Coil : " + Diameter);
    }
}
